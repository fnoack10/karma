# Karma: Nearest locations <h1> 
This application uses an MVVM architecture, to achive the goal of loading a set of locations, and organizing them based on how near they are to Karma's HQ.
A user is also able to follow/unfollow each location individually, and this values need to be persisted in the device's local memory.

RxSwift & RxCocoa are leveraged to implement the coordinator pattern Coordinator, as well as Interactor-View communication.
RealmSwift was selected to implement local storage on disk, for it's simple and straight forward api.
This frameworks were integrated via Cocoapods.

![Preview](preview.png)
