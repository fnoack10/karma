//
//  AppScene.swift
//  karma
//
//  Created by Franco Noack on 21.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

final class AppScene: Scene {
    
    // MARK: Properties
    var didFinish =  PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private let window: UIWindow
    private let api = APIGateway()
    private lazy var navigationController = UINavigationController()
    
    init(window: UIWindow) {
        self.window = window
        self.window.rootViewController = navigationController
    }
    
    func start() {
        launchLocationsScene()
    }
    
}

extension AppScene {
 
    private func launchLocationsScene() {
        let locationsScene = LocationsScene(api: api, navigationController: navigationController)
        locationsScene.start()
        addChild(locationsScene)
    }

}
