//
//  LocationsInteractor.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol LocationsInteractorInputs: InteractorInputs {
    func bind(actions: LocationsUI.Actions)
}

protocol LocationsInteractorUIOutputs: InteractorUIOutputs {
    var updateUI: Driver<LocationListPM> { get }
}

protocol LocationsInteractorType: Interactor {
    var inputs: LocationsInteractorInputs { get }
    var uiOutputs: LocationsInteractorUIOutputs { get }
}

typealias LocationsStream = Observable<LocationListPM>

final class LocationsInteractor: LocationsInteractorType, LocationsInteractorInputs, LocationsInteractorUIOutputs {
    
    //  MARK: Inputs
    func configure(with presentationModel: LocationListPM) {
        presentationModelSubject.accept(presentationModel)
    }
    
    func bind(actions: LocationsUI.Actions) {
        actions.follow.asObservable()
            .withLatestFrom(self.presentationModelSubject) { ($0, $1) }
            .subscribe(onNext: { [weak self] itemID, listPM in
                guard let strongSelf = self,
                    let itemPM = listPM.itemWithID(itemID) else { return }
                let newItemPM = strongSelf.updateFollowingStatus(for: itemPM)
                strongSelf.updatePresentationModel(with: newItemPM)
            }).disposed(by: disposeBag)
    }
    
    // MARK: Outputs
    var follow = PublishSubject<LocationListPM>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: UIOutputs
    lazy var updateUI: Driver<LocationListPM> = self.createUpdateUIStream()

    // MARK: I/O
    var inputs: LocationsInteractorInputs { return self }
    var uiOutputs: LocationsInteractorUIOutputs { return self }
    
    // MARK: Properties
    let disposeBag = DisposeBag()
    
    //  MARK: Private Properties
    private let gateway: LocationsGate
    private let presentationModelSubject = BehaviorRelay(value: LocationListPM())
    
    init(gateway: LocationsGate) {
        self.gateway = gateway
        
        bindUpdateStream()
    }
    
    private func bindUpdateStream() {
        loadLocationsStream()
            .flatMap { self.mergeWithLocalStorage($0) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    private func loadLocationsStream() ->  LocationsStream {
        return gateway.loadLocations()
            .map { result in
                switch result {
                case .success(let locations):
                    return LocationListPM(locations)
                case .failure(_):
                    // TODO: Handle error. Currently on error empty list is displayed.
                    return LocationListPM()
                }
        }
    }
    
    // MARK: Helper Methods
    
    private func updatePresentationModel(with itemPM: LocationItemPM) {
        var listPM = presentationModelSubject.value
        listPM.updateList(with: itemPM)
        presentationModelSubject.accept(listPM)
    }
    
    private func updateFollowingStatus(for locationItemPM: LocationItemPM) -> LocationItemPM {
        var updatedPM = locationItemPM
        updatedPM.isFollowing = !updatedPM.isFollowing
        RealmStorage.updateFollowedLocation(with: updatedPM.id, isFollowing: updatedPM.isFollowing)
        return updatedPM
    }
    
    private func createUpdateUIStream() -> Driver<LocationListPM> {
        return  presentationModelSubject.asDriver()
    }
    
}

// MARK: Local Storage
extension LocationsInteractor {
    
    private func mergeWithLocalStorage(_ locationListPM: LocationListPM) -> LocationsStream {
        let storedLocationIDs = RealmStorage.loadFollowedLocations() // Implementation Decision: If storage is empty, store API values locally. Otherwise, restore local values.
        return storedLocationIDs.isEmpty ? storeAPIValues(locationListPM) : restoreLocalValues(storedLocationIDs, locationListPM: locationListPM)
    }
    
    private func storeAPIValues(_  locationListPM: LocationListPM) -> LocationsStream {
        let apiLocationIDs = locationListPM.items
            .filter { $0.isFollowing == true }
            .map { $0.id }

        RealmStorage.storeFollowedLocations(with: apiLocationIDs)
        return Observable.just(locationListPM)
    }
    
    private func restoreLocalValues(_ storedLocationIDs: [Int], locationListPM: LocationListPM) -> LocationsStream {
        var restoredItems: [LocationItemPM] = []
        for item in locationListPM.items {
            var newItem = item
            newItem.isFollowing = storedLocationIDs.contains(item.id)
            restoredItems.append(newItem)
        }
        return Observable.just(LocationListPM(restoredItems))
    }
}
