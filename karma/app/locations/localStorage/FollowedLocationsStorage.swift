//
//  LocationsStorage.swift
//  karma
//
//  Created by Franco Noack on 23.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RealmSwift

protocol FollowedLocationsStorage {
    static func loadFollowedLocations() -> [Int]
    static func storeFollowedLocations(with ids: [Int])
    static func updateFollowedLocation(with id: Int, isFollowing: Bool)
}

final class FollowedLocation: Object {
    @objc dynamic var id: Int = 0
}

struct RealmStorage: FollowedLocationsStorage {

    static func loadFollowedLocations() -> [Int] {
        let realm = try! Realm()
        return Array(realm.objects(FollowedLocation.self)).map { $0.id }
    }
    
    static func storeFollowedLocations(with ids: [Int]) {
        DispatchQueue(label: "storeFollowedLocations").async {
            autoreleasepool {
                let realm = try! Realm()
                ids.forEach {
                    let item = FollowedLocation(value: ["id": $0])
                    try! realm.write {
                        realm.add(item)
                    }
                }
            }
        }
    }
    
    static func updateFollowedLocation(with id: Int, isFollowing: Bool) {
        DispatchQueue(label: "updateFollowedLocation").async {
            autoreleasepool {
                let realm = try! Realm()
                let item = realm.objects(FollowedLocation.self).filter("id == \(id)").first
                if isFollowing {
                    guard item == nil else { return }
                    let item = FollowedLocation(value: ["id": id])
                    try! realm.write {
                        realm.add(item)
                    }
                } else {
                    guard let item = item else { return }
                    try! realm.write {
                        realm.delete(item)
                    }
                }
            }
        }
    }
}
