//
//  Locations.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationList: JSONParsable {
    var items: [LocationItem] = []
}

struct LocationItem: JSONParsable {
    var id: Int
    var name: String
    var latitude: Double
    var longitude: Double
    var following: Bool
    
    var coordinates: CLLocation {
        guard let lat = CLLocationDegrees(exactly: latitude),
            let lon = CLLocationDegrees(exactly: longitude) else { return CLLocation() }
        return CLLocation(latitude: lat, longitude: lon)}
}

extension LocationItem {
    static let karma = LocationItem(id: 0, name: "Karma HQ", latitude: 59.330596, longitude: 18.0560967, following: false)
}
