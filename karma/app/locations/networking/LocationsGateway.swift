//
//  LocationsGateway.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

typealias LocationsResultStream = Observable<Result<LocationList, APIError>>

protocol LocationsGate {
    func loadLocations() -> LocationsResultStream
}

final class LocationsGateway: LocationsGate {
    
    private let api: APIGateway
    
    init(api: APIGateway) {
        self.api = api
    }
    
    func loadLocations() -> LocationsResultStream {
        return Observable.create { observer in
            self.api.request(with: LocationsRouting.loadLocations.urlRequest) { response in
                switch response {
                case .success(let data):
                    do {
                        let locations = try LocationList.parse(data)
                        observer.onNext(.success(locations))
                    } catch {
                        observer.onNext(.failure(.parsingFailed))
                    }
                case .failure(let error):
                    observer.onNext(.failure(error))
                }
            }
            return Disposables.create()
        }
    }
    
}
