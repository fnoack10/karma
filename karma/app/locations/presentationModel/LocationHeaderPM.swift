//
//  LocationSectionPM.swift
//  karma
//
//  Created by Franco Noack on 23.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import UIKit

struct LocationHeaderPM {
    var titleText: String
    var descriptionText: String
    var iconImage: UIImage?
    
    init(_ model: LocationItem) {
        titleText = model.name
        descriptionText = String(format: "%.5f, %.5f", model.latitude, model.longitude)
    }
}
