//
//  LocationItemPM.swift
//  karma
//
//  Created by Franco Noack on 22.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

struct LocationItemPM {
    let id: Int
    let titleText: String
    let descriptionText: String
    let distance: Double
    var isFollowing: Bool
    
    init(_ model: LocationItem) {
        id = model.id
        titleText = model.name
        distance = model.coordinates.distance(from: LocationItem.karma.coordinates)
        descriptionText = String(format: "%.2f km", distance/1000)
        isFollowing = model.following
    }
    
    var followButtonTitleText: String {
        return isFollowing ? "Following" : "Follow"
    }
    
}
