//
//  LocationListPM.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationListPM {
    var title: String = "Nearest Locations"
    var header: LocationHeaderPM = LocationHeaderPM(LocationItem.karma)
    var items: [LocationItemPM] = []
    
    init() {}
    
    init(_ model: LocationList) {
        self.items = model.items.map { LocationItemPM($0) }
        self.items.sort { $0.distance < $1.distance }
    }
    
    init(_ items: [LocationItemPM]) {
        self.items = items
    }
    
    func itemWithID(_ id: Int) -> LocationItemPM? {
        guard let item = items.first(where: { $0.id == id }) else { return nil }
        return item
    }
    
    mutating func updateList(with item: LocationItemPM) {
        guard let index = items.firstIndex(where: { $0.id == item.id }) else { return }
        items[index] = item
    }
    
}
