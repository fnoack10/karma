//
//  LocationsTheme.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import UIKit

struct LocationsTheme: Theme {

    static func apply(_ cell: LocationTableViewCell) {
        cell.selectionStyle = .none
        cell.titleLabel.textColor = Palette.titleTextColor
        cell.titleLabel.font = UIFont.systemFont(ofSize: 16)
        cell.descriptionLabel.textColor = Palette.textColor
        cell.descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        cell.followButton.setTitleColor(Palette.lightTextColor, for: .normal)
        cell.followButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.followButton.layer.cornerRadius = 4.0
        cell.followButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    static func update(_ followButton: UIButton, isFollowing: Bool) {
        followButton.backgroundColor = isFollowing ? Palette.followingButtonColor : Palette.followButtonColor
    }
    
    static func apply(_ header: LocationTableViewHeader) {
        header.backgroundColor = Palette.headerViewColor
        header.titleLabel.textColor = Palette.lightTextColor
        header.titleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        header.descriptionLabel.textColor = Palette.lightTextColor
        header.descriptionLabel.font = UIFont.systemFont(ofSize: 14)
    }
}
