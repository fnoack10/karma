//
//  LocationsUI.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LocationsUI: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Public Properties
    let disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private var interactor: LocationsInteractorType?
    private var items: [LocationItemPM] = []
    private lazy var header: LocationTableViewHeader = self.createLocationHeader()
    private lazy var followSubject = PublishSubject<Int>()
    
    static func create(with interactor: LocationsInteractorType) -> LocationsUI {
        let storyboard = UIStoryboard(name: "Locations", bundle: nil)
        let ui = storyboard.instantiateViewController(withIdentifier: "LocationsUI") as! LocationsUI
        ui.interactor = interactor
        return ui
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        setupUI()
    }
    
    private func applyTheme() {
        LocationsTheme.apply(self)
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60
        
        bindInteractor()
    }
        
    private func bindInteractor() {
        guard let interactor = interactor else { return }
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.updateUI(presentationModel)
        }).disposed(by: disposeBag)
    }
    
    private func updateUI(_ presentationModel: LocationListPM) {
        title = presentationModel.title
        items = presentationModel.items
        header.model = presentationModel.header
        tableView.reloadData()
    }
    
    private func createLocationHeader() -> LocationTableViewHeader {
        return LocationTableViewHeader.create()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func  tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return header
    }
    
    func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as! LocationTableViewCell
        let locationPM = items[indexPath.row]
        cell.presentationModel = locationPM
        cell.delegate = self
        return cell
    }

}

extension LocationsUI: LocationTableViewCellDelegate {
    func didTap(_ itemID: Int) {
        followSubject.onNext(itemID)
    }
}

extension LocationsUI {
    struct Actions {
        let follow: Driver<Int>
    }
    
    var actions: Actions {
        return Actions(follow: followSubject.asDriver(onErrorJustReturn: 0))
    }
}
