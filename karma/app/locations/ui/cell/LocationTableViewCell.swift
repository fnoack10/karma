//
//  LocationTableViewCell.swift
//  karma
//
//  Created by Franco Noack on 21.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import UIKit

protocol LocationTableViewCellDelegate: class {
    func didTap(_ itemID: Int)
}

class LocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var presentationModel: LocationItemPM? {
        didSet {
            updateUI()
        }
    }
    
    weak var delegate: LocationTableViewCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        applyTheme()
    }
    
    private func applyTheme() {
        LocationsTheme.apply(self)
    }
    
    private func  updateUI() {
        guard let pm = presentationModel else { return }
        titleLabel.text =  pm.titleText
        descriptionLabel.text = pm.descriptionText
        followButton.setTitle(pm.followButtonTitleText, for: .normal)
        LocationsTheme.update(followButton, isFollowing: pm.isFollowing)
    }
    
    @IBAction func didPressFollow(_ sender: UIButton) {
        guard let pm = presentationModel else { return }
        delegate?.didTap(pm.id)
    }
}


