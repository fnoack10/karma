//
//  LocationTableViewHeader.swift
//  karma
//
//  Created by Franco Noack on 23.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import UIKit

class LocationTableViewHeader: UIView {
 
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var model: LocationHeaderPM? {
        didSet { update() }
    }
    
    static func create() -> LocationTableViewHeader {
        return UINib(nibName: "LocationTableViewHeader", bundle: nil).instantiate(withOwner: self, options: nil).first! as! LocationTableViewHeader
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        applyTheme()
    }
    
    private func applyTheme() {
        LocationsTheme.apply(self)
    }
    
    func update() {
        guard let model = model else { return }
        titleLabel.text = model.titleText
        descriptionLabel.text = model.descriptionText
        self.layoutIfNeeded()
    }
    
}
