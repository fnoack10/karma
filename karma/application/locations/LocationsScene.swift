//
//  LocationsScene.swift
//  karma
//
//  Created by Franco Noack on 21.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

final class LocationsScene: Scene {
    
    // MARK: Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: Private Properties
    private let api: APIGateway
    private let navigationController: UINavigationController
    private lazy var gateway: LocationsGate = self.createLocationsGateway()
    
    // MARK: - Lifecycle
    init(api: APIGateway, navigationController: UINavigationController) {
        self.api = api
        self.navigationController = navigationController
    }
    
    func start() {
        launchLocationsUI()
    }
    
    private func createLocationsGateway() -> LocationsGate {
        return LocationsGateway(api: api)
    }
    
}

// MARK: UI
extension LocationsScene {
    
    private func launchLocationsUI() {

        let ui = UIStoryboard(name: "Locations", bundle: nil).instantiateViewController(withIdentifier: "LocationsUI") as! LocationsUI
        
        let interactor = LocationsInteractor(gateway: gateway)
        interactor.inputs.configure(with: LocationsPM())
        interactor.inputs.bind(actions: ui.actions)
        
//        interactor.outputs.login
//            .bind(to: self.didFinish)
//            .disposed(by: ui.disposeBag)
//
//        interactor.outputs.signup
//            .subscribe(onNext: { [weak self] _ in
//                self?.launchSignUpUI()
//            }).disposed(by: ui.disposeBag)
        
        navigationController.pushViewController(ui, animated: true)
    }
    
 
}
