//
//  LocationsInteractor.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol LocationsInteractorInputs: InteractorInputs {
    func configure(with presentationModel: LocationsPM)
    func bind(actions: LocationsUI.Actions)
}

protocol LocationsInteractorOutputs: InteractorOutputs {
    var follow: PublishSubject<LocationsPM> { get }
}

protocol LocationsInteractorUIOutputs: InteractorUIOutputs {
    var updateUI: Driver<LocationsPM> { get }
}

protocol LocationsInteractorType: Interactor {
    var inputs: LocationsInteractorInputs { get }
    var outputs: LocationsInteractorOutputs { get }
    var uiOutputs: LocationsInteractorUIOutputs { get }
}

typealias LocationsStream = Observable<LocationsPM>

final class LocationsInteractor: LocationsInteractorType, LocationsInteractorInputs, LocationsInteractorOutputs, LocationsInteractorUIOutputs {
    
    //  MARK: Inputs
    func configure(with presentationModel: LocationsPM) {
        presentationModelSubject.accept(presentationModel)
    }
    
    func bind(actions: LocationsUI.Actions) {
        
    }
    
    // MARK: Outputs
    var follow = PublishSubject<LocationsPM>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: UIOutputs
    lazy var updateUI: Driver<LocationsPM> = self.createUpdateUIStream()
    
    // MARK: I/O
    var inputs: LocationsInteractorInputs { return self }
    var outputs: LocationsInteractorOutputs { return self }
    var uiOutputs: LocationsInteractorUIOutputs { return self }

    // MARK: Properties
    let disposeBag = DisposeBag()
    
    //  MARK: Private Properties
    private let gateway: LocationsGate
    private let presentationModelSubject  =  BehaviorRelay(value: LocationsPM())
    
    init(gateway: LocationsGate) {
        self.gateway = gateway
        
        bindUpdateStream()
    }
    
    private func bindUpdateStream() {
        locationsUpdateStream()
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    private func locationsUpdateStream() -> LocationsStream {
        return Observable.merge(
            loadLocationsStream()
        )
    }
    
    private func loadLocationsStream() ->  LocationsStream {
        return gateway.loadLocations().map { _ in LocationsPM() }
    }
    
    private func createUpdateUIStream() -> Driver<LocationsPM> {
        return  presentationModelSubject.asDriver()
    }
    
}
