//
//  Locations.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

struct Location: JSONParsable {
    var id: Double
    var name: String
    var latitude: Double
    var longitude: Double
    var following: Bool
}

struct LocationsList: JSONParsable {
    var items: [Location] = []
}
