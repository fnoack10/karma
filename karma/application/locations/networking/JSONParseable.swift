//
//  JSONParseable.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

protocol JSONParsable: Decodable {
    static func parse(_ data: Data) throws -> Self
}

extension JSONParsable {
    static func parse(_ data: Data) throws -> Self {
        var json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments])
        if let items = json as? [Any] {
            json = ["items": items]
        }
        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        let model = try JSONDecoder().decode(Self.self, from: data)
        return model
    }
}
