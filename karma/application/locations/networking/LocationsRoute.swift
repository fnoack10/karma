//
//  LocationsGateway.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

enum LocationsRouting {
    case loadLocations
}

extension LocationsRouting: APIRouting {
    var baseURL: String { return "https://storage.googleapis.com" }
    var method: APIMethod { return .get }
    var path: String {
        switch self {
        case .loadLocations: return "/misc-internal/public/locations_filtered.json"
        }
    }
    var parameters: [String : Any]? {
        switch self {
        case .loadLocations: return nil
        }
    }
}
