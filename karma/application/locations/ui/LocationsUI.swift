//
//  LocationsUI.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LocationsUI: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    
    private lazy var followSubject = PublishSubject<Location?>()
    
    var locations: LocationsList = LocationsList()
    var subscription: Disposable?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        setupUI()
    }
    
    private func applyTheme() {
        
    }
    
    private func setupUI() {
        
        let api: APIGateway = APIGateway()
        let gateway: LocationsGateway = LocationsGateway(api: api)
        
        subscription = gateway.loadLocations()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                switch result {
                case .success(let locations):
                    self.locations = locations
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error.message)
                }
            })
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
        
    
    
    
    deinit {
        subscription?.dispose()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        let location = locations.items[indexPath.row]
        cell.textLabel?.text = location.name
        return cell
    }

}

extension LocationsUI {
    struct Actions {
        let follow: Driver<Location?>
    }
    
    var actions: Actions {
        return Actions(follow: followSubject.asDriver(onErrorJustReturn: nil))
    }
}
