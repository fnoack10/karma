//
//  Interactor.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

protocol InteractorInputs {}

protocol InteractorOutputs {
    var didFinish: PublishSubject<Void> { get }
}

protocol InteractorUIOutputs {}

protocol Interactor: InteractorInputs, InteractorOutputs, InteractorUIOutputs {
    var disposeBag: DisposeBag { get }
}
