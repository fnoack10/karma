//
//  APIError.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

enum APIError: Error {
    case parsingFailed
    case emptyResponse
    case requestFailed(String)
    
    var message: String {
        return "Some error happen"
    }
}
