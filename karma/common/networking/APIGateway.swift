//
//  APIGateway.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

typealias DataResponse = Result<Data, APIError>

final class APIGateway {
    private var urlSession = URLSession.shared
    
    init() {}
    
    func request(with request: URLRequest, completion: @escaping (DataResponse) -> Void) {
        urlSession.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(.requestFailed(error.localizedDescription)))
                return
            }
            
            guard let validData = data else {
                completion(.failure(.emptyResponse))
                return
            }
            
            completion(.success(validData))
        }.resume()
    }
}


