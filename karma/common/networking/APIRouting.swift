//
//  APIRouting.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation

enum APIMethod: String {
    case get = "GET"
}

protocol APIRouting {
    var baseURL: String { get }
    var path: String { get }
    var parameters: [String: Any]? { get }
    var method: APIMethod { get }
}

extension APIRouting {
    public var urlRequest: URLRequest {
        guard let  url = self.url else {
            fatalError("URL not found")
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
    
    private var url: URL? {
        var urlComponents = URLComponents(string: baseURL)
        urlComponents?.path = path
        return urlComponents?.url
    }
}
