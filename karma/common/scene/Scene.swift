//
//  Scene.swift
//  karma
//
//  Created by Franco Noack on 21.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import RxSwift

protocol Scene: class {
    var children: [Scene] { get set }
    var disposeBag: DisposeBag { get set }
    func start()
}

extension Scene {

    func addChild(_ scene: Scene) {
        if !children.contains(where: { $0 === scene }) {
            self.children.append(scene)
        }
    }
    
    func removeChild(_ scene: Scene) {
        self.children = self.children.filter { $0 !== scene }
    }
    
}
