//
//  Theme.swift
//  karma
//
//  Created by Franco Noack on 20.09.19.
//  Copyright © 2019 Franco Noack. All rights reserved.
//

import Foundation
import UIKit

protocol Theme {
    static func apply(_ viewController: UIViewController)
}

extension Theme {
    static func apply(_ viewController: UIViewController) {
        viewController.view.backgroundColor = Palette.backgroundColor
        viewController.navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: 17),
                                                                                  .foregroundColor: Palette.titleTextColor]
        if #available(iOS 11.0, *) {
            viewController.navigationController?.navigationBar.prefersLargeTitles = true
            viewController.navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: 30),
                                                                                           .foregroundColor: Palette.titleTextColor]
        }
    }
}

struct Palette {
    static var backgroundColor: UIColor = .white
    static var navigationTintColor: UIColor = .black
    static var titleTextColor: UIColor = .black
    static var textColor: UIColor = .gray
    static var lightTextColor:  UIColor = .white
    static var followButtonColor: UIColor = .black
    static var followingButtonColor: UIColor = .secondary
    static var headerViewColor: UIColor = .primary
}

extension UIColor {
    static var primary = UIColor(red: 250/255.0, green: 59/255.0, blue: 120/255.0, alpha: 1)
    static var secondary = UIColor(red: 52/255.0, green: 199/255.0, blue: 89/255.0, alpha: 1)
}
